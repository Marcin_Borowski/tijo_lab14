package pl.edu.pwsztar;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

final class UserId implements UserIdChecker {

    private final String id;    // NR. PESEL

    public UserId(final String id) {
        this.id = id;
    }

    @Override
    public boolean isCorrectSize() {
        return id.length()==11;
    }

    @Override
    public Optional<Sex> getSex() {
        if(id.charAt(9)%2==0){
            return Optional.of(Sex.WOMAN);}
        else {
            return Optional.of(Sex.MAN);
        }
    }

    @Override
    public boolean isCorrect() {
         List<Integer> controlNumber = Arrays.asList(9,7,3,1,9,7,3,1,9,7);
         int controlsum = 0;
         for(int i=0;i<10;i++)
         {controlsum += Character.getNumericValue(id.charAt(i))*controlNumber.get(i);
         }
         return controlsum%10 == Character.getNumericValue(id.charAt(10));
    }

    @Override
    public Optional<String> getDate() {
        int dzien;
        int miesiac;
        int rok;

        rok = 1900 + Character.getNumericValue(id.charAt(0))*10+Character.getNumericValue(id.charAt(1));
        if (Character.getNumericValue(id.charAt(2))>=2 && Character.getNumericValue(id.charAt(2))<8)
            rok+=Math.floor(Character.getNumericValue(id.charAt(2))/2)*100;
        if (Character.getNumericValue(id.charAt(2))>=8)
            rok-=100;

        miesiac = (Character.getNumericValue(id.charAt(2))%2)*10+Character.getNumericValue(id.charAt(3));
        dzien = Character.getNumericValue(id.charAt(4))*10+Character.getNumericValue(id.charAt(5));

        if(String.valueOf(dzien).length()==1 && String.valueOf(miesiac).length()==1){
            return Optional.of("0"+dzien+"-"+"0"+miesiac+"-"+rok);
        }
        if(String.valueOf(miesiac).length()==1){
            return Optional.of(dzien+"-"+"0"+miesiac+"-"+rok);
        }
        if(String.valueOf(dzien).length()==1){
            return Optional.of("0"+dzien+"-"+miesiac+"-"+rok);
        }
        return Optional.of(dzien+"-"+miesiac+"-"+rok);
    }
}
