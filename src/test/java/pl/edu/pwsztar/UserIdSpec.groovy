package pl.edu.pwsztar

import spock.lang.Specification
import spock.lang.Unroll

class UserIdSpec extends Specification {

    @Unroll
    def "should check correct size"() {
        given:
            def userid = new UserId(num)
        when:
            def iscorrect = userid.isCorrectSize()
        then:
            iscorrect == expected
        where:
            num || expected
        "98070806594" || true
        "95112566433" || true
        "920111044"   || false
        "990123444555"|| false
        ""            || false
    }

    @Unroll
    def "should get correct Sex"() {
        given:
        def userid = new UserId(num)
        when:
        def sex = userid.getSex()
        then:
        sex.get() == expected
        where:
        num || expected
        "98070806594" || UserIdChecker.Sex.MAN
        "95112566443" || UserIdChecker.Sex.WOMAN
        "99012344755" || UserIdChecker.Sex.MAN
    }

    @Unroll
    def "should check if Id structure is correct"() {
        given:
        def userid = new UserId(num)
        when:
        def isvalid = userid.isCorrect()
        then:
        isvalid == expected
        where:
        num || expected
        "98070806594" || true
        "95112566433" || false
        "86083136544" || true
    }

    @Unroll
    def "should return birthdate based on pesel"() {
        given:
        def userid = new UserId(num)
        when:
        def date = userid.getDate()
        then:
        date.get() == expected
        where:
        num || expected
        "98070806594" || "08-07-1998"
        "03320642138" || "06-12-2003"
        "46680926405" || "09-08-2246"
        "52811247112" || "12-01-1852"
    }
}
